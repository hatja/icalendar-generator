<?php

/*
 * Variables used in this script:
 * $summary     - text title of the event
 * $dateStart   - the starting date (in seconds since unix epoch)
 * $dateEnd     - the ending date (in seconds since unix epoch)
 * $address     - the event's address
 * $description - text description of the event
 * $filename    - the name of this file for saving (e.g. my-event-name.ics)
 */

namespace ical;

class ical {

    private $name;
    private $timezoneICal = 'Europe/Paris';
    private $dateStart;
    private $summary;
    private $dateEnd;
    private $filename;
    private $address;
    private $description;
    /**
     *
     * @var bool default false 
     */
    private $alarm        = FALSE;
    /**
     *
     * @var bool default false 
     */
    private $repeat       = FALSE;
    
    public function getName() {
        return $this->name;
    }

    public function getTimezoneICal() {
        return $this->timezoneICal;
    }

    public function getDateStart() {
        return $this->dateStart;
    }

    public function getSummary() {
        return $this->summary;
    }

    public function getDateEnd() {
        return $this->dateEnd;
    }

    public function getFilename() {
        return $this->filename;
    }

    public function getAddress() {
        return $this->address;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getAlarm() {
        return $this->alarm;
    }

    public function getRepeat() {
        return $this->repeat;
    }
    /**
     * 
     * @param type $dateEventStart
     * @return \ical\ical
     */
    public function setDateEventStart($dateEventStart) {
        $this->dateEventStart = $dateEventStart;
        return $this;
    }
    /**
     * 
     * @param type $dateEventEnd
     * @return \ical\ical
     */
    public function setDateEventEnd($dateEventEnd) {
        $this->dateEventEnd = $dateEventEnd;
        return $this;
    }
    /**
     * 
     * @param type $name
     * @return \ical\ical
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }
    /**
     * 
     * @param type $timezoneICal
     * @return \ical\ical
     */
    public function setTimezoneICal($timezoneICal) {
        $this->timezoneICal = $timezoneICal;
        return $this;
    }
    /**
     * 
     * @param \DateTime $dateStart
     * @return \ical\ical
     */
    public function setDateStart(\DateTime $dateStart) {
        $this->dateStart = $this->dateToCal($this->getHumanToUnix($dateStart->format('Y/m/d H:i:s')));
        return $this;
    }
    /**
     * 
     * @param type $summary
     * @return \ical\ical
     */
    public function setSummary($summary) {
        $this->summary = $summary;
        return $this;
    }
    /**
     * 
     * @param \DateTime $dateEnd
     * @return \ical\ical
     */
    public function setDateEnd(\DateTime $dateEnd) {
        $this->dateEnd = $this->dateToCal($this->getHumanToUnix($dateEnd->format('Y/m/d H:i:s')));
        return $this;
    }
    /**
     * 
     * @param type $filename
     * @return \ical\ical
     */
    public function setFilename($filename) {
        $this->filename = $filename;
        return $this;
    }
    /**
     * 
     * @param type $address
     * @return \ical\ical
     */
    public function setAddress($address) {
        $this->address = $address;
        return $this;
    }
    /**
     * 
     * @param type $description
     * @return \ical\ical
     */
    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }
    /**
     * 
     * @param type $alarm
     * @return \ical\ical
     * @throws \Exception
     */
    public function setAlarm($alarm) {

        if (is_int($alarm)) {
            $this->alarm = $alarm;
            return $this;
        } else {
            throw new \Exception(__CLASS__ . " : It's not an integer", 01);
        }
    }
    /**
     * 
     * @param type $repeat
     * @return \ical\ical
     */
    public function setRepeat($repeat) {
        $this->repeat = $repeat;
        return $this;
    }

    /**
     * @name getICAL()
     * @access public
     * @return string $iCal
     */
    public function getICAL() {

        $iCal = "BEGIN:VCALENDAR" . "\r\n";
        $iCal .= 'VERSION:2.0' . "\r\n";
        $iCal .= "PRODID:" . $this->getName() . "\r\n";
        $iCal .= "CALSCALE:GREGORIAN " . "\r\n";
        $iCal .= "BEGIN:VEVENT" . "\r\n";
        $iCal .= "DTSTART:" . $this->getDateStart() . "\r\n";
        $iCal .= "DTEND:" . $this->getDateEnd() . "\r\n";
        $iCal .= "SUMMARY:" . $this->escapeString($this->getSummary()) . "\r\n";
        $iCal .= 'UID:' . uniqid() . "\r\n";
        $iCal .= 'LOCATION: ' . $this->escapeString($this->getAddress()) . "\r\n";
        $iCal .= 'DESCRIPTION:' . $this->escapeString($this->getDescription()) . "\r\n";

        if ($this->getAlarm()) {
            $iCal .= 'BEGIN:VALARM' . "\r\n";
            $iCal .= 'ACTION:DISPLAY' . "\r\n";
            $iCal .= 'DESCRIPTION:Reminder' . "\r\n";
            $iCal .= 'TRIGGER:-PT' . $this->getAlarm() . 'M' . "\r\n";
            if ($this->getRepeat()) {
                $iCal .= 'REPEAT:' . $this->getRepeat() . "\r\n";
            }
            $iCal .= "END:VALARM" . "\r\n";
        }

        $iCal .= 'END:VEVENT' . "\r\n";
        $iCal .= 'END:VCALENDAR' . "\r\n";
        return $iCal;
    }

    /**
     * @name dateToCal()
     * @access private
     * @param \DateTime $timestamp
     * @return string
     */
    private function dateToCal(\DateTime $timestamp) {

        return  $timestamp->format('Ymd\THis\Z') ;
    }

    /**
     * @name escapeString()
     * @abstract Escapes a string of characters
     * @param string $string
     * @return string
     */
    private function escapeString($string) {
        return preg_replace('/([\,;])/', '\\\$1', $string);
    }

    /**
     * @name $addHeader();
     * @return headers and file
     */
    public function addHeader() {
        header('Content-type: text/calendar; charset=utf-8');
        header('Content-Disposition: attachment; filename=' . $this->getFilename() . '.ics');
    }

    /**
     * @name humanToUnix()
     * @param string $datestr like Y-m-d
     * @return bool
     * @return integer
     */
    private function getHumanToUnix($datestr = '')
    {
        if ($datestr === '') {
            return false;
        }
        $datestr = preg_replace('/\040+/', ' ', trim($datestr));
        //dd($datestr);
        /*if (!preg_match('/^(\d{2}|\d{4})\-[0-9]{1,2}\-[0-9]{1,2}\s[0-9]{1,2}:[0-9]{1,2}(?::[0-9]{1,2})?(?:\s[AP]M)?$/i', $datestr)) {
            return false;
        }*/
        sscanf($datestr, '%d/%d/%d %s %s', $year, $month, $day, $time, $ampm);

        sscanf($time, '%d:%d:%d', $hour, $min, $sec);
        isset($sec) or $sec = 0;
        if (isset($ampm)) {
            $ampm = strtolower($ampm);
            if ($ampm[0] === 'p' && $hour < 12) {
                $hour += 12;
            } elseif ($ampm[0] === 'a' && $hour === 12) {
                $hour = 0;
            }
        }

        $return = new \DateTime();
        $return->setTimestamp(mktime($hour, $min, $sec, $month, $day, $year));
        return $return;
    }

}
