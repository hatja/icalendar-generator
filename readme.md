# iCalendar generator
This simple class generate a *.ics file. require php >= 5.4.2.
## Install with composer
```
require "calendar/icsfile": "dev-master"
```
## Usage 

```php

<?php  
    require_once 'vendor/autoload.php';

    use ical\ical;
try {
 
        $ical = (new ical())->setAddress('Paris')
                ->setDateStart(\DateTime())
                ->setDateEnd(\DateTime())
                ->setDescription('wonder description')
                ->setSummary('Running')
                ->setFilename(uniqid());
        $ical->getHeader();
       
    echo $ical->getICAL();
            
  
        } catch (Exception $exc) {
            echo $exc->getMessage();
}
 

```